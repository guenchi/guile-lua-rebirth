;;  Copyright (C) 2013,2014,2019
;;      "Mu Lei" known as "NalaGinrut" <NalaGinrut@gmail.com>
;;  This file is free software: you can redistribute it and/or modify
;;  it under the terms of the GNU General Public License as published by
;;  the Free Software Foundation, either version 3 of the License, or
;;  (at your option) any later version.

;;  This file is distributed in the hope that it will be useful,
;;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;  GNU General Public License for more details.

;;  You should have received a copy of the GNU General Public License
;;  along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (language lua spec)
  #:use-module (system base language)
  #:use-module (system base target)
  #:use-module (language lua utils)
  #:use-module (language lua parser)
  #:use-module (language lua compile-tree-il)
  #:use-module (language scheme decompile-tree-il)
  #:export (lua))

(define-language lua
  #:title "Lua"
  #:reader (lambda (port _) (read-lua port))
  #:compilers `((tree-il . ,compile-tree-il))
  #:decompilers `((tree-il . ,decompile-tree-il))
  #:evaluator   (lambda (x module) (primitive-eval x))
  #:printer write
  #:make-default-environment (lambda ()
                               (let ((m (make-fresh-user-module)))
                                 (purify-module! m)
                                 (module-clear! m)
                                 ;; Provide a separate `current-reader' fluid so that
                                 ;; compile-time changes to `current-reader' are
                                 ;; limited to the current compilation unit.
                                 (module-define! m 'current-reader (make-fluid))

                                 ;; Default to `simple-format', as is the case until
                                 ;; (ice-9 format) is loaded.  This allows
                                 ;; compile-time warnings to be emitted when using
                                 ;; unsupported options.
                                 (module-define! m 'format (@ (guile) simple-format))

                                 m)))
