;;  Copyright (C) 2019
;;      "Mu Lei" known as "NalaGinrut" <NalaGinrut@gmail.com>
;;  This file is free software: you can redistribute it and/or modify
;;  it under the terms of the GNU General Public License as published by
;;  the Free Software Foundation, either version 3 of the License, or
;;  (at your option) any later version.

;;  This file is distributed in the hope that it will be useful,
;;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;  GNU General Public License for more details.

;;  You should have received a copy of the GNU General Public License
;;  along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (language lua stdlib string)
  #:use-module (ice-9 match)
  #:use-module (ice-9 format)
  #:use-module (language lua utils)
  #:export (primitive:string-sub
            primitive:string-format
            primitive:string-char
            primitive:string-find))

(define *charset* (string->char-set "qscdEefgGiouXx~%"))
(define *replace-table*
  `((#\q . #\s)
    (#\s . #\a)
    (#\~ . #\~)
    (#\% . #\%)))

(define (primitive:string-format fmt . args)
  (lambda ()
    (values
     (call-with-output-string
      (lambda (output)
        (call-with-input-string
         fmt
         (lambda (input)
           (let lp ((status 'out) (cnt 0))
             (cond
              ((eof-object? (peek-char input)) #t)
              ((char=? (peek-char input) #\%)
               (read-char input)
               (display #\~ output)
               (lp 'in cnt))
              ((and (eq? status 'in)
                    (char-set-contains? *charset* (peek-char input)))
               (let* ((ch (read-char input))
                      (sub (assoc-ref *replace-table* ch)))
                 (cond
                  (sub
                   (display sub output))
                  ((char=? ch #\c)
                   (format output "~ac" (list-ref args cnt)))
                  (else (display ch output)))
                 (lp 'out (1+ cnt))))
              ((eq? status 'in)
               (let ((ch (read-char input)))
                 (if (eqv? ch #\.)
                     (display #\, output)
                     (display ch output))
                 (lp status cnt)))
              (else
               (display (read-char input) output)
               (lp status cnt))))))))
     args)))

(define (primitive:string-sub str start end)
  (substring/shared str start end))

(define (lua-string-sub args)
  (->stdlib-call 'string 'primitive:string-sub args))

(define (lua-string-format args)
  (->stdlib-call 'string 'primitive:string-format args))

(define string-primitives
  `(("__string_sub" . ,lua-string-sub)
    ("__string_format" . ,lua-string-format)))
