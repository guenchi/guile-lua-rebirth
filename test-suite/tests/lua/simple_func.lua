function test1(a,b)
   return a+b;
end

function test2(...)
   print(...);
end

function test3(x, y, ...)
   return x+y;
end

function test4(x, ...)
   return ...;
end

print("test1");
print(test1(1,2));

print("test2");
test2(1,2,3);

print("test3");
print(test3(1,2,3));

print("test4");
print(test4(1,2,3));
